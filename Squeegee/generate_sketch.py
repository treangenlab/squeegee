import subprocess
import os

def run_mash(meta_data, output, THREADS, kmer_size=21, min_kmer_copy_num=2, sketch_size=100000):
    sketch_dir = os.path.join(output, "mash_sketches")
     
    if not os.path.exists(sketch_dir):
        os.mkdir(sketch_dir)

    for sample_id in meta_data:
        sketch_file = os.path.join(sketch_dir, sample_id)
        temp_fastq = os.path.join(output, "temp.fastq")

        read_file_1 = meta_data[sample_id][1]
        read_file_2 = meta_data[sample_id][2]
        
        os.system(f"cat {read_file_1} {read_file_2} > {temp_fastq}")

        subprocess.run(["mash", "sketch",
                        "-s", str(sketch_size),
                        "-k", str(kmer_size),
                        "-m", str(min_kmer_copy_num),
                        "-r", temp_fastq,
                        "-p", str(THREADS),
                        "-o", sketch_file])

        if os.path.exists(temp_fastq):
            os.remove(temp_fastq)
            
def calc_mash_dist(meta_data, output):
    sketch_dir = os.path.join(output, "mash_sketches")
    sample_list = meta_data.keys()

    # need parallelization 
    with open(os.path.join(output, "dist_matrix.txt"), "w") as dist_matrix:
        for sample_id1 in sample_list:
            dist_matrix.write(f"{sample_id1}\t")
            for sample_id2 in sample_list:
                dry_run = subprocess.check_output(["mash", "dist",
                                                   os.path.join(sketch_dir, f"{sample_id1}.msh"),
                                                   os.path.join(sketch_dir, f"{sample_id2}.msh")])

                lines = dry_run.splitlines()
                dist = lines[0].decode("utf-8").split("\t")[2]
                dist_matrix.write(f"{dist}\t")
            dist_matrix.write(f"\n")
