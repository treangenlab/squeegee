import os
import subprocess
from statistics import mean, median
from collections import defaultdict
import numpy as np
from Bio import SeqIO

def calculate_depth(meta_type, output_dir):
    type_depth = os.path.join(output_dir, "type_depth_files")
    if not os.path.exists(type_depth):
        os.mkdir(type_depth)

    for sample_type in meta_type:
        depth_file = os.path.join(type_depth, f"{sample_type}.depth")
        #print(depth_file)
        with open("bam_file_list.txt", "w") as bam_file_list:        
            for sample in meta_type[sample_type]:
                bam_file_list.write(os.path.join(output_dir, "bam_files", f"{sample}.sorted.bam")+"\n")
                
        os.system(f"samtools depth -f bam_file_list.txt -g SECONDARY > {depth_file}")
        
        if os.path.exists("bam_file_list.txt"):
            os.remove("bam_file_list.txt")
    
def sort_samfile(meta_data, output_dir, num_cores):
    '''converting and sorting alignment files'''
    bam_files = os.path.join(output_dir, "bam_files")
    sam_files = os.path.join(output_dir, "sam_files")
    
    # depth_files = os.path.join(output_dir, "depth_files")
    if not os.path.exists(bam_files):
        os.mkdir(bam_files)

    for sample_id in meta_data:
        # covert sam file to binary bam file
        subprocess.run([
            "samtools",
            "view",
            "-@", str(num_cores),
            "-bS", os.path.join(sam_files, f"{sample_id}.sam"),
            "-o", os.path.join(bam_files, f"{sample_id}.bam")],
                       check=True)
        # sort the bam file 
        subprocess.run([
            "samtools",
            "sort",
            "-@", str(num_cores),
            "-o", os.path.join(bam_files, f"{sample_id}.sorted.bam"),
            "-O", "BAM", os.path.join(bam_files, f"{sample_id}.bam")],
                       check=True)
        # indexing the sorted bam file 
        subprocess.run([
            "samtools",
            "index",
            os.path.join(bam_files, f"{sample_id}.sorted.bam")],
                       check=True)

def read_mapping_bowtie2(reference_file, meta_data, num_cores, output_dir):
    '''map the reads to the reference'''
    sam_output_dir = os.path.join(output_dir, "sam_files")
    if not os.path.exists(sam_output_dir):
        os.mkdir(sam_output_dir)

    for sample_id in meta_data:
        read_file_1 = os.path.join(output_dir, "masked_reads", f"{sample_id}.match.1.fastq")
        read_file_2 = os.path.join(output_dir, "masked_reads", f"{sample_id}.match.2.fastq")
        subprocess.run([
                "bowtie2",
                "-p", str(num_cores),
                "--local",
                "--quiet",
                "-a",
                "--no-unal",
                "--maxins", "600",
                "-x", os.path.join(output_dir, "bowtie2_ref_index", "reference"),
                "-1", read_file_1,
                "-2", read_file_2,
                "-S", os.path.join(sam_output_dir, f"{sample_id}.sam")],
            check=True)

def bowtie2_index(reference_file, output_dir):
    '''create bowtie2 index'''
    bowtie_output_dir = os.path.join(output_dir, "bowtie2_ref_index")
    if not os.path.exists(bowtie_output_dir):
        os.mkdir(bowtie_output_dir)

    # build bowtie2 reference index
    subprocess.run([
            "bowtie2-build",
            "--quiet",
            reference_file,
            os.path.join(bowtie_output_dir, "reference")],
        check=True)

def calc_depth_matrix(meta_type, ref_fasta, output_dir):
    genome_length = dict()
    genome_name = dict()
    genome_list = []

    with open(ref_fasta, "r") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            #print(len(record.seq), record.description)
            genome_length[record.id] = len(record.seq)
            genome_name[record.id] = record.description
            genome_list.append(record.id)

    type_depth = os.path.join(output_dir, "type_depth_files")

    genome_coverage = np.zeros((len(genome_list), len(meta_type)), dtype=float)
    genome_depth = np.zeros((len(genome_list), len(meta_type)), dtype=float)

    row_header = []
    for sample_type in meta_type:
        depth_file = os.path.join(type_depth, f"{sample_type}.depth")
        
        type_index = len(row_header)
        row_header.append(sample_type)
        
        genome_pos_count = defaultdict(int)
        genome_totol_count = defaultdict(int)
        
        with open(depth_file, "r") as depth:
            lines = depth.readlines()
            for line in lines:
                genome_id = line.split("\t")[0]
                pos = int(line.split("\t")[1])
                depths = line.strip().split("\t")[2:]
                depths_int = [int(elem) for elem in depths]

                depth = sum(depths_int)

                if depth >= 3:
                    genome_pos_count[genome_id] += 1
                    genome_totol_count[genome_id] += depth
            
        for genome_idx, genome_id in enumerate(genome_list):
            genome_coverage[genome_idx][type_index] = genome_pos_count[genome_id]/genome_length[genome_id]
            if genome_pos_count[genome_id] != 0:
                genome_depth[genome_idx][type_index] = genome_totol_count[genome_id]/genome_pos_count[genome_id]

    with open(os.path.join(output_dir, "genome_coverage.txt"), "w") as genome_coverage_output:
        for type_idx,sample_type in enumerate(row_header):
            genome_coverage_output.write("\t"+sample_type)
        genome_coverage_output.write("\n")
            
        for genome_idx,genome_id in enumerate(genome_list):
            genome_coverage_output.write(f"{genome_id}")
            for type_idx,sample_type in enumerate(row_header):
                genome_coverage_output.write(f"\t{genome_coverage[genome_idx][type_idx]}")
            genome_coverage_output.write("\n")

    with open(os.path.join(output_dir, "genome_depth.txt"), "w") as genome_coverage_output:
        for type_idx,sample_type in enumerate(row_header):
            genome_coverage_output.write("\t"+sample_type)
        genome_coverage_output.write("\n")
            
        for genome_idx,genome_id in enumerate(genome_list):
            genome_coverage_output.write(f"{genome_id}")
            for type_idx,sample_type in enumerate(row_header):
                genome_coverage_output.write(f"\t{genome_depth[genome_idx][type_idx]}")
            genome_coverage_output.write("\n")

def parse_potiential_contaminants(potiential_contaminants):
    with open(potiential_contaminants, "r") as potiential_contaminants_input:
        lines = potiential_contaminants_input.readlines()
        taxids = []
        taxname = []
        for line in lines:
            if not line.startswith("#"):
                taxid = line.strip().split("\t")[-1]
                name = line.strip().split("\t")[1]
                taxids.append(taxid)
                taxname.append(name)
    return taxids, taxname

def parse_distance_matrix(distance_matrix_file):
    with open(distance_matrix_file, "r") as distance_matrix_input:
        lines = distance_matrix_input.readlines()
        sample_id_list = []
        sample_id_dict = dict()
        for line in lines:
            sample_id = line.split("\t")[0]
            dist_list = line.strip().split("\t")[1:]
            sample_id_list.append(sample_id)
            sample_id_dict[sample_id] = [float(i) for i in dist_list]
    return sample_id_list, sample_id_dict

def get_presence_matrix(meta_data, taxid_list, sample_id_list, min_reads, min_abundance, output_dir):
    presence_matrix = np.zeros((len(taxid_list), len(sample_id_list)), dtype = bool)
    
    for sample_idx, sample_id in enumerate(sample_id_list):
        with open(f"{output_dir}/kraken_report/{sample_id}.report", "r") as kraken_report:
            lines = kraken_report.readlines()
            total_reads = 0
            for line in lines:
                taxon_id = line.split("\t")[4]
                read_count = int(line.split("\t")[1])
                taxon_name = line.split("\t")[-1].strip()
                taxon_rank = line.split("\t")[3]
                
                # get total number of reads by adding classified and unclassified reads
                if taxon_id == "0":
                    total_reads += read_count
                elif taxon_id == "1":
                    total_reads += read_count  
                elif (read_count >= min_reads or read_count/total_reads >= min_abundance) and taxon_id in taxid_list:
                    taxon_idx = taxid_list.index(taxon_id)
                    presence_matrix[taxon_idx][sample_idx] = True
    return presence_matrix

def calc_distance(presence_matrix, sample_id_list, sample_id_dict, taxid_list):
    taxon_distance_dict = defaultdict(list)
    for taxon_idx, taxon_id in enumerate(taxid_list):
        for sample_idx1, sample_id in enumerate(sample_id_list):
            for sample_idx2, sample_id in enumerate(sample_id_list[sample_idx1+1:]):
                if presence_matrix[taxon_idx][sample_idx1] and presence_matrix[taxon_idx][sample_idx2]:
                    mash_distance = sample_id_dict[sample_id][sample_idx2]
                    taxon_distance_dict[taxon_id].append(mash_distance)
                    
    return taxon_distance_dict