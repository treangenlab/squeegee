import os
import subprocess
import time

def run_kraken(sample_id, raw_read_1, raw_read_2, KRAKEN_DB, OUTPUT_DIR, THREADS):
    kraken_output_dir = os.path.join(OUTPUT_DIR, "kraken_output")
    kraken_report_dir = os.path.join(OUTPUT_DIR, "kraken_report")

    if not os.path.exists(kraken_output_dir):
            os.mkdir(kraken_output_dir)
    if not os.path.exists(kraken_report_dir):
        os.mkdir(kraken_report_dir)

    output = os.path.join(kraken_output_dir, f"{sample_id}.output")
    report = os.path.join(kraken_report_dir, f"{sample_id}.report")

    print(f"Running Kraken with {THREADS} threads on {sample_id}.")
    
    subprocess.run(["kraken",
                    "-db", KRAKEN_DB,
                    "--paired",
                    raw_read_1,
                    raw_read_2,
                    "--threads", str(THREADS),
                    "--output", output])

    os.system(f"kraken-report -db {KRAKEN_DB} {output} > {report}")




