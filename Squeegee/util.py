import os
from collections import defaultdict
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

def parse_meta_data(META_DATA):
    # parsing metadata into multiple dict()
    # store metadata for each sample
    meta_data = defaultdict(list)
    # store metadata for each sample type
    meta_type = defaultdict(list)
    with open(META_DATA, "r") as meta:
        for line in meta.readlines():
            sample_id = line.strip().split("\t")[0]
            sample_type = line.strip().split("\t")[1]
            sample_read_1_abs_dir = line.strip().split("\t")[2]
            sample_read_2_abs_dir = line.strip().split("\t")[3]

            meta_data[sample_id] = [sample_type, sample_read_1_abs_dir, sample_read_2_abs_dir]
            meta_type[sample_type].append(sample_id)

    return meta_data, meta_type


def fetch_genomes(potiential_contaminants, KRAKEN_DB, OUTPUT_DIR):
    # Assembly summaries
    # TODO: check existence of summary files
    bac_assembly_summary = os.path.join(KRAKEN_DB, "library", "bacteria", "assembly_summary.txt")
    vir_assembly_summary = os.path.join(KRAKEN_DB, "library", "viral",    "assembly_summary.txt")
    arc_assembly_summary = os.path.join(KRAKEN_DB, "library", "archaea",  "assembly_summary.txt")
    assembly_summaries = [bac_assembly_summary, vir_assembly_summary, arc_assembly_summary]

    # Squenge DBs
    # TODO: check existence of DBs
    bac_sequences = os.path.join(KRAKEN_DB, "library", "bacteria", "library.fna")
    vir_sequences = os.path.join(KRAKEN_DB, "library", "viral",    "library.fna")
    arc_sequences = os.path.join(KRAKEN_DB, "library", "archaea",  "library.fna")
    sequence_dbs = [bac_sequences, vir_sequences, arc_sequences]

    spcid2taxid = defaultdict(list)
    for assembly_summary in assembly_summaries:
        with open(assembly_summary, "r") as mapping:
            for line in mapping:
                if not line.startswith("#"):
                    taxid = line.split("\t")[5]
                    spcid = line.split("\t")[6]
                    spcid2taxid[spcid].append(taxid)

    taxid2genomeid = defaultdict(list)
    with open(os.path.join(KRAKEN_DB, "seqid2taxid.map"), "r") as mapping:
        for line in mapping:
            info = line.split("\t")[0]
            taxid = info.split("|")[1]
            genome = info.split("|")[2]
            taxid2genomeid[taxid].append(genome)

    genome_list = defaultdict(list)
    genome2spcid = dict()
    
    for spcid in potiential_contaminants:     
        # check whether seqid2taxid.map contains a reference genome
        genome_list[spcid] = taxid2genomeid[spcid]
        #print("taxid:", spcid, genome_list)
        if len(genome_list[spcid]) > 0:
            for genome in genome_list[spcid]:
                genome2spcid[genome] = spcid
        else:
        # seqid2taxid.map does not contain a reference genome
            strains = spcid2taxid[spcid]
            for strain in strains:
                genome_list[spcid] = genome_list[spcid] + taxid2genomeid[strain]
                for genome in taxid2genomeid[strain]:
                    genome2spcid[genome] = spcid

    genomes = []
    for sequence_db in sequence_dbs:
        for record in SeqIO.parse(sequence_db, "fasta"):
            genome_id = record.id.split("|")[-1]
            if genome_id in genome2spcid and not "plasmid" in record.description and genome2spcid[genome_id] in potiential_contaminants:
                renamed_record = SeqRecord(
                                    record.seq,
                                    id = genome2spcid[genome_id] + "|" + record.id.split("|")[-1],
                                    name = genome2spcid[genome_id] + "|" + record.id.split("|")[-1],
                                    description=record.description.lstrip("kraken:taxid|"))
                print(renamed_record)
                genomes.append(renamed_record)
                potiential_contaminants.remove(genome2spcid[genome_id])

            if len(potiential_contaminants) == 0:
                break

    SeqIO.write(genomes, os.path.join(OUTPUT_DIR, "potiential_contaminants_genomes.fasta"), "fasta")
    
    return potiential_contaminants
