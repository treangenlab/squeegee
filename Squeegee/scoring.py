'''
Combine contamination evidence (breadth and depth of genome coverage,
mash distance between samples that contains the candidates, and prevalence
rate of each candidate species) to make final predictions.
'''

import os
import argparse
from collections import defaultdict
import numpy as np
from Squeegee import util

def parse_prevalence(min_prevalence, output_dir):
    '''parse prevalence score of each candidate contaminants'''
    taxid2name = dict()
    taxid2prevalence = dict()
    sum_prevalence = 0
    with open(os.path.join(output_dir, "potiential_contaminants.txt"), "r") as prevalence_input:
        lines = prevalence_input.readlines()
        for line in lines:
            if not line.startswith("#"):
                tax_id = line.strip().split("\t")[2]
                tax_name = line.strip().split("\t")[1]
                tax_prevalence = float(line.strip().split("\t")[0])
                if tax_prevalence >= min_prevalence:
                    taxid2name[tax_id] = tax_name
                    taxid2prevalence[tax_id] = tax_prevalence
                    sum_prevalence += tax_prevalence
    # also return the mean of the prevalence score
    return taxid2name, taxid2prevalence, sum_prevalence/len(taxid2prevalence)

def get_prevalence_type(meta_data, meta_type, taxid2prevalence, sample_types,
                        output_dir, min_reads=30, min_abundance=0.0005):
    prevalence_type = dict()
    for taxid in taxid2prevalence:
        prevalence_type[taxid] = np.zeros(len(sample_types), dtype=float)

    taxon_prevalence_dict = defaultdict(float)
    taxon_name_dict = dict()

    for sample_id in meta_data:
        sample_dir = os.path.join(output_dir, "kraken_report", f"{sample_id}.report")
        sample_type = meta_data[sample_id][0]
        #print(sample_types.index(sample_type))
        with open(sample_dir, "r") as report:
            lines = report.readlines()
            total_reads = 0
            for line in lines:
                taxon_id = line.split("\t")[4]
                read_count = int(line.split("\t")[1])
                taxon_name = line.split("\t")[-1].strip()
                taxon_rank = line.split("\t")[3]

                # get total number of reads by adding classified and unclassified reads
                if taxon_id == "0":
                    total_reads += read_count
                elif taxon_id == "1":
                    total_reads += read_count
                elif (read_count >= min_reads or read_count/total_reads >= min_abundance) \
                    and taxon_id in taxid2prevalence:
                    taxon_name_dict[taxon_id] = taxon_name
                    #print(taxon_id, prevalence_type[taxon_id][sample_types.index(sample_type)])
                    prevalence_type[taxon_id][sample_types.index(sample_type)] += 1

    for taxon_id in prevalence_type:
        for i in range(len(sample_types)):
            prevalence_type[taxon_id][i] /= len(meta_type[sample_types[i]])

    return prevalence_type

def parse_mash_score(taxid2prevalence, output_dir):
    taxid2mashscore = dict()
    taxid2mashscore_max = dict()
    mash_scores = []
    with open(os.path.join(output_dir, "mash_score.txt"), "r") as mash_score_input:
        lines = mash_score_input.readlines()
        for line in lines:
            if not line.startswith("#"):
                tax_id = line.strip().split("\t")[2]
                if tax_id in taxid2prevalence:
                    tax_mashscore = float(line.strip().split("\t")[0])
                    tax_mashscore_max = float(line.strip().split("\t")[1])
                    taxid2mashscore[tax_id] = tax_mashscore
                    taxid2mashscore_max[tax_id] = tax_mashscore_max
                    mash_scores.append(tax_mashscore)
    return taxid2mashscore, taxid2mashscore_max, np.mean(mash_scores)

def parse_alignment_score(taxid2prevalence, output_dir):
    taxid2alignscore = dict()
    taxid2refid = dict()
    sample_type = []
    with open(os.path.join(output_dir, "genome_coverage.txt"), "r") as genome_coverage:
        lines = genome_coverage.readlines()
        sample_type = lines[0].strip().split("\t")
        for line in lines[1:]:
            if not line.startswith("#"):
                tax_id = line.strip().split("\t")[0].split("|")[0]
                if tax_id in taxid2prevalence:
                    ref_id = line.strip().split("\t")[0].split("|")[1]
                    taxid2alignscore[tax_id] = [float(i) for i in line.strip().split("\t")[1:]]
                    taxid2refid[tax_id] = ref_id
    return taxid2alignscore, taxid2refid, sample_type

def parse_depth_score(taxid2prevalence, output_dir):
    taxid2depth = dict()
    with open(os.path.join(output_dir, "genome_depth.txt"), "r") as genome_depth:
        lines = genome_depth.readlines()
        for line in lines[1:]:
            if not line.startswith("#"):
                tax_id = line.strip().split("\t")[0].split("|")[0]
                if tax_id in taxid2prevalence:
                    ref_id = line.strip().split("\t")[0].split("|")[1]
                    taxid2depth[tax_id] = [float(i) for i in line.strip().split("\t")[1:]]
    return taxid2depth

def final_prediction(meta_data_file, output_dir,
                     min_prevalence=0.6, min_score=0.75,
                     min_align=0.075, stacked_idx=6,
                     min_reads=30, min_abundance=0.0005):
    meta_data, meta_type = util.parse_meta_data(meta_data_file)
    sample_types = list(meta_type.keys())
    sample_types.sort()

    # combined prevalence score
    taxid2name, taxid2prevalence, prevalence_mean = parse_prevalence(min_prevalence, output_dir)
    prevalence_type = get_prevalence_type(meta_data, meta_type,
                                          taxid2prevalence, sample_types,
                                          output_dir, min_reads, min_abundance)

    taxid2mashscore, taxid2mashscore_max, mash_mean = parse_mash_score(taxid2prevalence, output_dir)
    taxid2alignscore, taxid2refid, sample_type = parse_alignment_score(taxid2prevalence, output_dir)
    taxid2depth = parse_depth_score(taxid2prevalence, output_dir)

    result_failed = []
    result_passed = []
    for taxid in taxid2name:
        tax_name = taxid2name[taxid]
        prevalence = taxid2prevalence[taxid]
        mash_score = taxid2mashscore[taxid]
        mash_score_max = taxid2mashscore_max[taxid]
        depths = taxid2depth[taxid]
        align_scores = taxid2alignscore[taxid]
        prevalence_t = prevalence_type[taxid]

        mash_score_combined = mash_score
        align_score_combined = min(np.mean(align_scores)/(min_align*5), 1)
        prevalence_score_combined = prevalence

        combined = (mash_score_combined/mash_mean + \
            align_score_combined + \
            prevalence_score_combined/prevalence_mean)/3

        align_filter = int(np.median(depths)/np.median(align_scores) < 10**stacked_idx \
                       and max(align_scores) >= min_align)
        min_filter = int(combined >= min_score)

        if align_filter and min_filter and prevalence >= min_prevalence:
            result_passed.append([taxid, tax_name] + list(prevalence_t) + list(align_scores) +
                                 [align_score_combined, prevalence_score_combined,
                                  mash_score_combined, combined, align_filter, min_filter, 1])
        else:
            result_failed.append([taxid, tax_name] + list(prevalence_t) + list(align_scores) +
                                 [align_score_combined, prevalence_score_combined,
                                  mash_score_combined, combined, align_filter, min_filter, 0])

        with open(os.path.join(output_dir, "final_predictions.txt"), "w") as output_f:
            sample_types_str = "/".join(sample_types)
            output_f.write(f"# Sample_type: {sample_types_str}\n")
            output_f.write(f"# taxid\ttax_name\tcombined_score\tprevalence_score\talign_scores\tmash_score\tsample_type_prevalence\tsample_type_coverage\n")
            for item in result_passed:
                taxid = item[0]
                tax_name = item[1]
                combined = item[-4]
                prevalence_score = item[-6]
                align_scores = item[-7]
                mash_score = item[-5]
                sample_type_prevalence = "/".join(str(round(x,4)) for x in item[2:2+len(sample_types)])
                sample_type_coverage = "/".join(str(round(x,4)) for x in item[2+len(sample_types):2+2*len(sample_types)])
                output_f.write(f"{taxid}\t{tax_name}\t{combined}\t{prevalence_score}\t{align_scores}\t{mash_score}\t{sample_type_prevalence}\t{sample_type_coverage}\n")

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Scoring and final predictions.")
    parser.add_argument("metadata", type=str, help="input matadata in txt format")
    parser.add_argument("output", type=str, help="squeegee output directory")
    parser.add_argument("--min-prevalence", type=float, default=0.6,
                        help="Minimum prevalence threshold for a species to \
                              be indentified as a contaminant species.")
    parser.add_argument("--min-score", type=float, default=0.75,
                        help="Minimum contaminant score threshold for a species \
                              to be indentified as a contaminant species.")
    parser.add_argument("--min-align", type=float, default=0.075,
                        help="Minimum breadth of genome coverge threshold for \
                              a species to be indentified as a contaminant species.")
    parser.add_argument("--stacked-idx", type=int, default=6,
                        help="Index to determine whether or not aligned reads \
                              have been stacked in small region.")
    args = parser.parse_args()

    meta_data_file = args.metadata
    output_dir = args.output
    min_prevalence = args.min_prevalence
    min_score = args.min_score
    min_align = args.min_align
    stacked_idx = args.stacked_idx

    final_prediction(meta_data_file, output_dir, min_prevalence, min_score, min_align, stacked_idx)

if __name__ == "__main__":
    main()
