import os, sys
import subprocess

def build_mdb(reference_fasta, output_dir, threads, kmer_size=28):
    mdb_prefix = os.path.join(output_dir, "candidates_mdb")
    subprocess.call(["meryl",
                     "-B",
                     "-threads", str(threads),
                     "-s", reference_fasta,
                     "-o", mdb_prefix,
                     "-m", str(kmer_size)])
    
    return mdb_prefix

def run_kmer_mask(meta_data, mdb_prefix, output_dir, threads, kmer_size=28):
    masked_reads = os.path.join(output_dir, "masked_reads")
    if not os.path.exists(masked_reads):
        os.mkdir(masked_reads)

    for sample_id in meta_data:
        read_file_1 = meta_data[sample_id][1]
        read_file_2 = meta_data[sample_id][2]

        out_prefix = os.path.join(masked_reads, sample_id)
        subprocess.call(["kmer-mask",
                        "-ms", str(kmer_size),
                        "-mdb", mdb_prefix,
                        "-1", read_file_1,
                        "-2", read_file_2,
                        "-clean", "0.0",
                        "-match", "0.01",
                        "-nomasking", 
                        "-t", str(threads),
                        "-o", out_prefix])

        clean_1 = f"{out_prefix}.clean.1.fastq"
        clean_2 = f"{out_prefix}.clean.2.fastq"
        match_1 = f"{out_prefix}.match.1.fastq"
        match_2 = f"{out_prefix}.match.2.fastq"
        murky_1 = f"{out_prefix}.murky.1.fastq"
        murky_2 = f"{out_prefix}.murky.2.fastq"
        mixed_1 = f"{out_prefix}.mixed.1.fastq"
        mixed_2 = f"{out_prefix}.mixed.2.fastq"

        if os.path.exists(clean_1):
            os.remove(clean_1)
        if os.path.exists(clean_2):
            os.remove(clean_2)
        if os.path.exists(murky_1):
            os.remove(murky_1)
        if os.path.exists(murky_2):
            os.remove(murky_2)
        if os.path.exists(mixed_1):
            os.remove(mixed_1)
        if os.path.exists(mixed_2):
            os.remove(mixed_2)